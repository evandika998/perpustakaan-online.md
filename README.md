### Perpustakaan Berbasis Online
```mermaid
    classDiagram
    class User {
        +login()
        +register()
        +searchBook()
        +borrowBook()
        +returnBook()
        +readBook()
    }
    
    class Admin {
        +login()
        +manageBooks()
        +manageMembers()
        +confirmActivities()
        +generateReport()
    }
    
    class Library {
        +books: List<Book>
        +members: List<Member>
        +login(user: User)
        +login(admin: Admin)
        +searchBook(keyword: String): List<Book>
        +borrowBook(user: User, book: Book): boolean
        +returnBook(user: User, book: Book): boolean
        +readBook(user: User, book: Book): boolean
        +addBook(book: Book)
        +updateBook(book: Book)
        +deleteBook(book: Book)
        +addMember(member: Member)
        +updateMember(member: Member)
        +deleteMember(member: Member)
        +confirmActivity(activity: Activity)
        +generateReport(): Report
    }
    
    class Book {
        -title: String
        -author: String
        -publisher: String
        -year: int
        -available: boolean
        +getTitle(): String
        +getAuthor(): String
        +getPublisher(): String
        +getYear(): int
        +isAvailable(): boolean
    }
    
    class Member {
        -name: String
        -email: String
        -address: String
        +getName(): String
        +getEmail(): String
        +getAddress(): String
    }
    
    class Activity {
        -user: User
        -action: String
        -timestamp: DateTime
        +getUser(): User
        +getAction(): String
        +getTimestamp(): DateTime
    }
    
    class Report {
        -activities: List<Activity>
        +getActivities(): List<Activity>
    }
    
    User --> Library
    Admin --> Library
    Library "1" *-- "*" Book
    Library "1" *-- "*" Member
    Library --> Activity
    Library --> Report
```
